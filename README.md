# ACoreChip

This repository contains the top level module for the A-Core chip-level Chisel implementation. A developers' [tutorial](https://gitlab.com/a-core/a-core_documentation/tutorials/-/blob/master/a-core_chisel-dev-guide.md) is available to get you started with the A-Core hardware development.

`ACoreChip` is an SoC implementation that consists of the `ACoreBase` RV32I[M] processor core,
program and random access memories, and any other desired memory mapped peripherals. The high-level
architecture is shown below:

![ACoreChip architecture diagram](diagrams/acorechip.png)

## Notable features:
* RISC-V Instruction Set Architecture (RV32I[M])
* Harvard architecture
* Pipelined with 7 stages
* JTAG programming interface
* ISA extensions can be independently toggled on/off in configuration
* Memory mapped IO and peripherals with an automatically generated AXI4-Lite interconnect
* Generates a verilog descripion of the design for implementation platform tools

For more detailed description of ACoreBase see the README in its own repository.

## Quickstart
The following sequence of commands can be used to generated the verilog description of the design:
```bash
$ ./init_submodules.sh
$ ./configure
$ make ACoreChip
```
On success, the verilog description will be generated to `./verilog/acorechip.v`.

## Configuration file
A-Core uses a YAML-based configuration file for configuration options. The flow generates a configuration class, `ACoreConfig`, from the YAML file. The default configuration file is in this repository, called `core-default.yml`.

### ACoreConfig
See [a_core_common API reference](https://a-core.gitlab.io/a-core_chisel/a_core_common/a_core_common/ACoreConfig.html)

## Linting and Formatting
This project uses `scalafmt` for formatting code. Using a formatter ensures that the entire code base looks clean and coherent. Use it, so that other people reading your code have less headaches.

`scalafmt` is used as the integrated formatter in Metals, which is the Scala-extension for IDE's like VS Code and IntelliJ. When using one of these tools, formatting your code happens by pressing one button. In VS Code with Metals, a file can be formatted with the command `Format document`.

Formatting can also be run from the command line, as long as `sbt-scalafmt`-plugin is included in the project in `/project/plugins.sbt`. In this way, the formatting can be run with sbt tasks. The different task keys can be found in https://scalameta.org/scalafmt/docs/installation.html#task-keys. For example, formatting all files in the project is accomplished by running `sbt scalafmtAll`.

Convenience methods are provided as recipes in the `Makefile`. After running `./configure`, a signle file can be formatted with the command `make format_file file_path=path/to/file.scala`. Alternatively, it is possible to format all files in the project with `make format_all`.

The rules for formatting are written in `.scalafmt.conf`. The rules are not perfect: if you have suggestions about adding new rules or editing the existing ones, feel free to suggest. Only a subset of the rules are in use at the moment: more rules can be found at https://scalameta.org/scalafmt/docs/configuration.html

Sometimes, it can be desirable to bypass the formatter. The tool can sometimes do a bad job, or some special cases can be formatted more beautifully by hand. If you feel this is the case, you can escape the formatting tool by using decorators like this:
```
// format: off
your-code-here
// format: on
```

### [API Documentation](https://a-core.gitlab.io/a-core_chisel/ACoreChip/)
