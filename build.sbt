import scala.sys.process._
// OBS: sbt._ has also process. Importing scala.sys.process
// and explicitly using it ensures the correct operation

ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / version          := scala.sys.process.Process("git rev-parse --short HEAD").!!.mkString.replaceAll("\\s", "")+"-SNAPSHOT"
ThisBuild / organization     := "Chisel-blocks"

// Suppresses eviction errors for new sbt versions
ThisBuild / evictionErrorLevel := Level.Info

resolvers += "A-Core Gitlab" at "https://gitlab.com/api/v4/groups/13348068/-/packages/maven"

val chiselVersion = "3.5.6"

// Sets amba and a_core_common version for the whole ACoreChip project,
// including sub-projects like ACoreBase and ProgMem
val ambaVersion = settingKey[String]("The version of amba used for building.")
val accVersion  = settingKey[String]("The version of a_core_common used for building")

ThisBuild / ambaVersion := "0.9+"
ThisBuild / accVersion  := "1.0+"

lazy val acorebase = (project in file("ACoreBase"))
  .settings(
    ambaVersion := (ThisBuild / ambaVersion).value,
    accVersion  := (ThisBuild / accVersion).value
  )

lazy val jtag = (project in file("JTAG"))

lazy val icache = (project in file ("icache"))

lazy val asyncqueue = (project in file ("asyncfifo"))

lazy val acorechip = (project in file("."))
  .settings(
    name := "acorechip",
    libraryDependencies ++= Seq(
      "edu.berkeley.cs" %% "chisel3" % chiselVersion,
      "edu.berkeley.cs" %% "chiseltest" % "0.5.1",
      "edu.berkeley.cs" %% "chisel-iotesters" % "2.5.0",
      "Chisel-blocks"   %% "amba" % ambaVersion.value,
      "Chisel-blocks"   %% "a_core_common" % accVersion.value
    ),
    scalacOptions ++= Seq(
      "-language:reflectiveCalls",
      "-deprecation",
      "-feature",
      "-Xcheckinit",
      "-P:chiselplugin:genBundleElements",
    ),
    addCompilerPlugin("edu.berkeley.cs" % "chisel3-plugin" % chiselVersion cross CrossVersion.full),
  )
  .dependsOn(acorebase, jtag, icache, asyncqueue)
  .aggregate(acorebase)

// Parse the version of a submodle from the git submodule status
// for those modules not version controlled by Maven or equivalent
def gitSubmoduleHashSnapshotVersion(submod: String): String = {
    val shellcommand =  "git submodule status | grep %s | awk '{print substr($1,0,7)}'".format(submod)
    scala.sys.process.Process(Seq("/bin/sh", "-c", shellcommand )).!!.mkString.replaceAll("\\s", "")+"-SNAPSHOT"
}

// Put your git-version controlled snapshots here
// libraryDependencies += "Chisel-blocks" %% "someblock" % gitSubmoduleHashSnapshotVersion("someblock")
