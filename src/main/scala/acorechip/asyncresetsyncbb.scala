package acorechip

import chisel3._
import chisel3.util._
import chisel3.experimental._

/** Invert active low async reset and synchronize its deassertation
  * Use this to synchronize asyncronous resets from top-level IO
  */
class AsyncResetSyncBB(activeLow: Boolean = false) extends BlackBox with HasBlackBoxInline {
  val io = IO(new Bundle {
    val clock       = Input(Clock())        // system clock
    val areset_in   = Input(AsyncReset())   // async reset from external world with configurable polarity
    val areset_sync = Output(AsyncReset())  // active high async reset with synchronized deassertion
  })
  val polarity = if (activeLow) "negedge" else "posedge"
  val level = if (activeLow) "!areset_in" else "areset_in"

  override def desiredName = s"AsyncResetSyncBB_$polarity"

  setInline(
    s"AsyncResetSyncBB_$polarity.v",
    s"""
    |module AsyncResetSyncBB_$polarity(
    |  input  wire clock,
    |  input  wire areset_in,
    |  output wire areset_sync
    |);
    |  reg first, second;
    |  always @(posedge clock or $polarity areset_in)
    |    if ($level) begin
    |      first <= 1'b1;
    |      second <= 1'b1;
    |    end else begin
    |      first <= 1'b0;
    |      second <= first;
    |    end
    |  assign areset_sync = second;
    |endmodule
    """.stripMargin
  )
}
