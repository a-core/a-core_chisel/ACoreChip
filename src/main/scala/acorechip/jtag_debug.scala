package acorechip

import chisel3._
import chisel3.util._
import chisel3.experimental._
import asyncqueue.modules._
import amba.axi4l._
import jtag._
import jtag.config.JtagConfig
import a_core_common.ACoreConfig

case class JTAGDebugIO[T <: JTAG, U <: AXI4LIO](jtag_inst: T) extends Bundle {
  val tapio = chiselTypeOf(jtag_inst.tapio)
  val core_en = Output(Bool())
}

class JTAGDebug(jtag_config: JtagConfig, core_config: ACoreConfig) extends Module {

  val jtag = Module(new JTAG(config=jtag_config)) 

  val core_en                  = jtag_config.tdrs.find(_.name == "core_en").get
  val databus_iface_addr       = jtag_config.tdrs.find(_.name == "databus_iface_addr").get
  val databus_iface_read_en    = jtag_config.tdrs.find(_.name == "databus_iface_read_en").get
  val databus_iface_read_data  = jtag_config.tdrs.find(_.name == "databus_iface_read_data").get
  val databus_iface_write_data = jtag_config.tdrs.find(_.name == "databus_iface_write_data").get
  val databus_iface_write_en   = jtag_config.tdrs.find(_.name == "databus_iface_write_en").get

  val axi4l_if = Module(new AXI4LMasterInterface(core_config.addr_width, core_config.data_width))
  val axi4l = IO(chiselTypeOf(axi4l_if.io.axi4l))

  axi4l <> axi4l_if.io.axi4l

  val io = IO(JTAGDebugIO(jtag_inst = jtag))

  // Synchronize read and write enable signals from JTAG to core clock domain
  val syncedRen = AsyncResetSynchronizerShiftReg(in = jtag.tdrio.out(databus_iface_read_en.ir), sync = 3, init = false.B, name = Option("cdc_reg"))
  val syncedWen = AsyncResetSynchronizerShiftReg(in = jtag.tdrio.out(databus_iface_write_en.ir), sync = 3, init = false.B, name = Option("cdc_reg"))

  // Synchronize core_en
  val syncedCen = AsyncResetSynchronizerShiftReg(in = jtag.tdrio.out(core_en.ir), sync = 3, init = false.B, name = Option("cdc_reg"))

  // Synchronize read/write address by using either read_en or write_en
  val addrHolder      = RegEnable(jtag.tdrio.out(databus_iface_addr.ir), syncedRen.asBool || syncedWen.asBool)
  // Synchronize write data by using write_en
  val writeDataHolder = RegEnable(jtag.tdrio.out(databus_iface_write_data.ir), syncedWen.asBool)
  // Store read data when it is valid
  val readDataHolder  = RegEnable(axi4l_if.io.core.read.data.bits, axi4l_if.io.core.read.data.valid)
  // Synchronize read valid to JTAG clock domain
  val syncedRValid    = withClock (io.tapio.TCK) { ShiftRegister(axi4l_if.io.core.read.data.valid, 3) }
  // Synchronize read data to JTAG clock domain using synchronized read valid signal
  val readDataSync    = withClock (io.tapio.TCK) { RegEnable(readDataHolder, syncedRValid) }

  // Req cannot be deasserted before gnt - these ensure that won't happen
  val wReqHolder = RegInit(false.B)
  val rReqHolder = RegInit(false.B)

  when (axi4l_if.io.core.write.req && !axi4l_if.io.core.write.gnt) {
    wReqHolder := true.B
  } .elsewhen(axi4l_if.io.core.write.req && axi4l_if.io.core.write.gnt) {
    wReqHolder := false.B
  }

  when (axi4l_if.io.core.read.req && !axi4l_if.io.core.read.gnt) {
    rReqHolder := true.B
  } .elsewhen(axi4l_if.io.core.read.req && axi4l_if.io.core.read.gnt) {
    rReqHolder := false.B
  }

  axi4l_if.io.clk_rst.ACLK    := clock
  axi4l_if.io.clk_rst.ARESETn := !reset.asBool
  axi4l_if.io.core.read.addr  := addrHolder
  axi4l_if.io.core.write.addr := addrHolder
  axi4l_if.io.core.read.req   := RegNext(syncedRen).orR || rReqHolder
  axi4l_if.io.core.write.req  := RegNext(syncedWen).orR || wReqHolder
  axi4l_if.io.core.write.data := writeDataHolder
  axi4l_if.io.core.write.mask := "hF".U

  jtag.tdrio.in := DontCare
  jtag.tdrio.in(jtag_config.tdrs.find(_.name == "databus_iface_read_data").get.ir) := readDataSync

  io.core_en := syncedCen

  jtag.tapio <> io.tapio
  // Connect TRSTn if defined
  if (jtag_config.has_trstn.getOrElse(false)) {
    // syncronize async reset deassertion
    val trstn_sync = Module(new AsyncResetSyncBB(activeLow = true))
    trstn_sync.io.clock     := clock
    trstn_sync.io.areset_in := io.tapio.TRSTn.get
    jtag.tapio.TRSTn.get    := trstn_sync.io.areset_sync
  }

}
