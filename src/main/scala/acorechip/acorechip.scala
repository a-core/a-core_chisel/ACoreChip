// SPDX-License-Identifier: Apache-2.0

// Chisel module ACoreChip
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2021-05-27

package acorechip

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._
import a_core_common.util._
import acorebase._
import icache._
import amba.axi4l._
import amba.common._
import jtag._
import jtag.config._

import java.nio.file.Paths
import scala.collection.immutable.ListMap

/** IO definitions for ACoreChip */
class ACoreChipIO(hasTRSTn: Boolean = false, 
                  enable_debug: Boolean = false,
                  tx_en: Boolean = false,
                  rx_en: Boolean = false
  ) extends Bundle {
  // Jtag IOs
  val jtag = Flipped(new TAPIO(hasTRSTn))
  // Core error flag
  val core_fault = Output(Bool())
  // Memory mapped IO
  val gpi = Input(UInt(32.W))
  val gpo = Output(UInt(32.W))
  // uart
  val tx = if (tx_en) Some(Output(Bool())) else None
  val rx = if (rx_en) Some(Input(Bool()))  else None
  // Debug interface
  val debug = if (enable_debug) Some(DebugIO()) else None
}

/**
  * Module definition for ACoreChip
  *
  * @param config ACoreConfig configuration class
  * @param jtag_config JTAG Config
  * @param gen_cheaders Generate C header files
  * @param cheaders_target_dir Target directory for generated C header files
  */
class ACoreChip(
  config: ACoreConfig,
  jtag_config: JtagConfig,
  gen_cheaders: Boolean = false,
  cheaders_target_dir: Option[String] = None
) extends Module with genCHeaders {

  override def desiredName = "acorechip"

  val ch_target_dir = cheaders_target_dir.getOrElse(System.getProperty("user.dir"))

  val uart_tx_en = true
  val uart_rx_en = false

  val io = IO(new ACoreChipIO(hasTRSTn=jtag_config.has_trstn.getOrElse(false), enable_debug=config.debug, tx_en=uart_tx_en, rx_en=uart_rx_en))

  val clk_rst = Wire(new AXI4LClkRst)
  clk_rst.ACLK    := clock
  clk_rst.ARESETn := !(reset.asBool)

  //=========================================== Chip modules ===========================================

  val jtag              = Module(new JTAGDebug(jtag_config, config))
  val core              = Module(new ACoreBase(config=config, enable_rvfi=false))
  val progmem           = Module(new AXI4LSRAM(addr_width=config.addr_width, data_width=config.data_width, mem_depth=config.progmem_depth-2, if_type="dual-port"))
  val ram               = Module(new AXI4LSRAM(addr_width=config.addr_width, data_width=config.data_width, mem_depth=config.datamem_depth-2, if_type="dual-port"))
  val gpio              = Module(new AXI4LGPIO(addr_width=config.addr_width, data_width=config.data_width, withSync=true))
  val uart              = Module(new AXI4LUART(addr_width=config.addr_width, data_width=config.data_width, tx_en=uart_tx_en, rx_en=uart_rx_en, tx_fifo_depth=8))
  val errorSlave        = Module(new AXI4LError(addr_width=config.addr_width, data_width=config.data_width))
  // Arbiter to arbitrate progmem access between instruction fetch and crossbar
  val progmemArb        = Module(new AXI4LMasterArbiter(2, config.addr_width, config.data_width, "round-robin"))
  // Cut to combinational path (crossbar -> progmem) to improve timing
  val progmemCut        = Module(new AXI4LCut(config.addr_width, config.data_width))
  // lazy val because we define memory_map later
  lazy val interconnect = Module(new AXI4LXBar(addr_width=32, data_width=32, n_masters=(config.n_masters + 1), memory_map, arb_type = "round-robin"))

  // Memory map for AXI4L Crossbar
  // errorSlave must be index 0
  val default_memory_map = Seq[MemoryRange] (
    MemoryRange(begin=0, end=config.pc_init.U.litValue-1, mode=MemoryMode.RW, name="AXI4LERROR"),
    MemoryRange(begin=config.pc_init, depth=config.progmem_depth, mode=MemoryMode.R, name="PROGMEM"),
    MemoryRange(begin="h2000_0000", depth=config.datamem_depth, mode=MemoryMode.RW, name="AXI4LSRAM"),
    MemoryRange(begin="h3000_0000", depth=5, mode=MemoryMode.RW, name="AXI4LGPIO"),
    MemoryRange(begin="h3000_1000", depth=4, mode=MemoryMode.W, name="AXI4LUART"),
  )

  // Entries to be added to the crossbar memory map
  var extra_memory_map = List[MemoryRange]()

  // Store extra masters to a list
  var master_list = List[AXI4LDMA]()

  // Optional direct memory access (DMA) modules
  var dma_use = false
  for ((dma, i) <- config.memory_bus.extra_masters.dmas.zipWithIndex) {
    if (dma.use) {
      dma_use = true
      val dma = Module(new AXI4LDMA(addr_width=config.addr_width,
                                    data_width=config.data_width,
                                    fifo_depth=config.memory_bus.extra_masters.dmas(i).fifo_depth,
                                    increment_depth=config.memory_bus.extra_masters.dmas(i).increment_depth))
      master_list = master_list :+ dma
      val dma_name: String = {
        if (i == 0) "AXI4LDMA"
        else s"AXI4LDMA_$i"
      } 
      extra_memory_map = extra_memory_map :+ (MemoryRange(begin=config.memory_bus.extra_slaves.dma_controllers(i).address,
                                      depth = 6, name=dma_name))
      dma.clk_rst := clk_rst
      dma.io.start_trigger := 0.U
      // One header file for all dmas is enough if they don't have any separating features
      if (gen_cheaders) genCHeaders(dma.memory_map, Paths.get(ch_target_dir, "/acore-dma.h").toString(), "dma", false)
    }
  }

  // Construct the final crossbar memory map
  val memory_map: Seq[MemoryRange] = default_memory_map ++ extra_memory_map

  //================================== Connections between modules ==================================== 
  
  interconnect.clk_rst := clk_rst
  
  // Connect axi4l masters to crossbar
  jtag.axi4l      <> interconnect.slave_ports(0)
  core.io.dmem    <> interconnect.slave_ports(1)
  for ((module,i) <- master_list.zipWithIndex) {
    interconnect.slave_ports(i+2) <> module.io.axi4l
    module.clk_rst := clk_rst
  }

  progmemArb.slave_ports(0) <> progmemCut.master_port

  // Connect axi4l slaves to crossbar
  interconnect.master_ports(0) <> errorSlave.slave_port
  interconnect.master_ports(1) <> progmemCut.slave_port
  interconnect.master_ports(2) <> ram.slave_port
  interconnect.master_ports(3) <> gpio.slave_port
  interconnect.master_ports(4) <> uart.slave_port
  for ((module,i) <- master_list.zipWithIndex) {
    interconnect.master_ports(i+5) <> module.slave_port
    module.clk_rst := clk_rst
  }

  // Connect clocks and resets
  errorSlave.clk_rst := clk_rst
  progmemArb.clk_rst := clk_rst
  progmemCut.clk_rst := clk_rst
  progmem.clk_rst    := clk_rst
  ram.clk_rst        := clk_rst
  gpio.clk_rst       := clk_rst
  uart.clk_rst       := clk_rst

  core.io.core_en := jtag.io.core_en

  progmem.slave_port <> progmemArb.master_port
  core.io.ifetch     <> progmemArb.slave_ports(1)
  // Convert to local address
  // Can we get rid of this?
  progmemArb.slave_ports(1).AR.bits.ADDR := core.io.ifetch.AR.bits.ADDR - config.pc_init.U

  val use_icache = false

  if (use_icache) {
    /* TODO - modify icache to use AXI4L input
    val icacheConfig = ICacheConfig(
      fetch_addr_width = 32,
      fetch_data_width = 32,
      refill_data_width = 32,
      nb_ways = 4,
      cache_size = 4096,
      cache_line = 4
    )
    val icache = Module(new ICache(icacheConfig))
    core.io.ifetch.rdata_valid := icache.io.fetch_rvalid_o
    core.io.ifetch.rdata       := icache.io.fetch_rdata_o
    core.io.ifetch.gnt         := icache.io.fetch_gnt_o
    core.io.ifetch.fault       := icache.io.fetch_fault_o

    icache.io.fetch_addr_i     := core.io.ifetch.raddr
    icache.io.fetch_req_i      := core.io.ifetch.req

    icache.io.refill_gnt_i     := progmem.io.mem_gnt
    icache.io.refill_rvalid_i  := progmem.io.mem_rdata_valid
    icache.io.refill_rdata_i   := progmem.io.mem_rdata
    icache.io.refill_fault_i   := progmem.io.fault
    progmem.io.mem_raddr       := icache.io.refill_addr_o
    progmem.io.mem_req         := icache.io.refill_req_o
  */
  } else {
    //core.io.ifetch.rdata_valid := progmem.io.mem_rdata_valid
    //core.io.ifetch.rdata       := progmem.io.mem_rdata
    //core.io.ifetch.gnt         := progmem.io.mem_gnt
    //core.io.ifetch.fault       := progmem.io.fault
    //progmem.io.mem_raddr       := core.io.ifetch.raddr
    //progmem.io.mem_req         := core.io.ifetch.req
  }


  //================================= Connections to external world =================================== 

  // gpio
  io.gpo := gpio.io.out_vec
  gpio.io.in_vec := io.gpi

  // uart
  io.tx.get := uart.io.tx
  uart.io.rx := io.rx.getOrElse(true.B)

  io.core_fault := core.io.core_fault
  if (config.debug) {
    io.debug.get.instruction_text := core.io.debug.get.instruction_text
  }

  // JTAG TAP  
  io.jtag <> jtag.io.tapio

  if (gen_cheaders) {
    genCHeaders(memory_map,      Paths.get(ch_target_dir, "/a-core.h").toString(), "a_core", false)
    genCHeaders(gpio.memory_map, Paths.get(ch_target_dir, "/acore-gpio.h").toString(), "gpio", false)
    genCHeaders(uart.memory_map, Paths.get(ch_target_dir, "/acore-uart.h").toString(), "uart", false)
  }

}

/** This gives you verilog */
object ACoreChip extends App with a_core_common.OptionParser {
  // Parse command-line arguments
  val default_opts : Map[String, String] = Map(
    "-core_config"         ->  "core-default.yml",
    "-jtag_config"         ->  "jtag-config.yml",
    "-isa_string"          -> "",
    "-cheaders_target_dir" ->  "include",
    "-gen_cheaders"        ->  "false"
  )
  val help = Seq(
    ("-core_config",         "String",   "A-Core configuration YAML file. Default \"core-default.yml\"."),
    ("-jtag_config",         "String",   "JTAG configuration YAML file path. Default \"jtag-config.yml\"."),
    ("-isa_string",          "String",  "Manually set isa_string (e.g. \"rv32imfc\")."),
    ("-cheaders_target_dir", "String",   "Path where to save generated include files. Default \"include\""),
    ("-gen_cheaders",        "Boolean",  "Generate C Headers for compatible modules (e.g. AXI4LSlave's). Default \"false\".")
  )
  val (options, arguments) = getopts(default_opts, args.toList, help)
  printopts(options, arguments)

  val core_config_file = options("-core_config")
  val core_config: ACoreConfig = ACoreConfig.loadFromFile(core_config_file)

  val isa_string = options("-isa_string")
  
  if (isa_string != "") {
    core_config.extension_set = ACoreConfig.parseExtensionSet(isa_string)
  }

  core_config.validate()

  println(core_config)

  // Parse JTAG configuration from a YAML file
  val jtag_config_file = options("-jtag_config")
  var jtag_config: Option[JtagConfig] = None
  JtagConfig.loadFromFile(jtag_config_file) match {
    case Left(config) => jtag_config = Some(config)
    case Right(err) => {
      System.err.println(s"\nCould not load JTAG configuration from file:\n${err.msg}")
      System.err.println("Run your JTAG configuration YAML through jtag-config-lint for more comprehensive checks!")
      System.err.println("TODO: link to jtag-config-lint repository")
      System.exit(-1)
    }
  }

  val annos = Seq(ChiselGeneratorAnnotation(() => new ACoreChip(
      config=core_config,
      jtag_config=jtag_config.get,
      gen_cheaders=options("-gen_cheaders").toBoolean,
      cheaders_target_dir=Option(options("-cheaders_target_dir"))
  )))
  (new ChiselStage).execute(arguments.toArray, annos)
}

