# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles
from elftools.elf.elffile import ELFFile
import os

def read_elf(filepath):
    """
    Load program data to memory from the ELF file located at ``filepath``.
    The loaded data is stored as an numpy array with adderss and data columns.

    As a side effect this method sets the ``self.jtag_steps`` variable to contain
    the time required by the JTAG programming sequence.
    

    Parameters
    ----------
    filepath : str
        Path to program ELF file to be loaded.

    """
    
    print("Reading program data from %s" % os.path.abspath(filepath))
    elffile = None
    try:
        fd = open(filepath, 'rb')
        elffile = ELFFile(fd)
    except Exception as e:
        print("Could not open %s : %s" %(filepath, e))

    # Get program instructions as a byte string
    text_section = elffile.get_section_by_name('.text')
    instruction_bytes = text_section.data()
    fd.close()
    return instruction_bytes

@cocotb.test()
async def test_pipeline(dut):
    """
    This test loads the program from an elf file into program memory, toggles reset, and runs the clock
    for a specified number of clock cycles
    """
    elf_path = cocotb.plusargs["elf"]
    if not elf_path:
        raise Exception("Please provide an elf file. Example: make ELF_FILE=/path/to/elf.elf")
    # In elf, the program is stored in an array of bytes in little-endian format
    elf = read_elf(elf_path)

    clock = dut.clock
    reset = dut.reset
    cocotb.start_soon(Clock(clock, 1, units="us").start())

    # Write to progmem slots
    for i in range(0, int(len(elf)/4)):
        dut.progmem.mem_array_0[0x00 + i].value = elf[i*4]
        dut.progmem.mem_array_1[0x00 + i].value = elf[i*4 + 1]
        dut.progmem.mem_array_2[0x00 + i].value = elf[i*4 + 2]
        dut.progmem.mem_array_3[0x00 + i].value = elf[i*4 + 3]

    reset.value = 0
    dut.core_io_core_en.value = 0
    await ClockCycles(clock, 2)
    reset.value = 1
    await ClockCycles(clock, 2)
    dut.core_io_core_en.value = 1
    reset.value = 0
    await ClockCycles(clock, 1000)
