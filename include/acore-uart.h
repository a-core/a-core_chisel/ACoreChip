// Automatically generated Thu Jun 13 10:15:09 EEST 2024

#ifndef UART_MMAP_H
#define UART_MMAP_H

#define UART_TX_CLK_THRESH      0x0       

/*
||Transmit data byte. Writing to this address will push it to the transmit FIFO.|Transmit starts automatically if tx_en is 1.|If the FIFO is full, the write will be stalled, if transmitter is enabled, until it is not full.If transmitter isn't enabled, then old (not-transmitted) data is overwritten.
*/
#define UART_TX_BYTE            0x4       

#define UART_TX_CTRL            0x8       
#define UART_TX_CTRL_INIT       0x1       
/*
Enable transmitter. By default, it is enabled
*/
#define UART_TX_CTRL_TX_EN_OFFSET 0x0       
#define UART_TX_CTRL_TX_EN_MASK 0x1       
#define UART_TX_CTRL_TX_EN_INIT 0x1       

#define UART_TX_STATUS          0xc       
/*
Transmit in progress
*/
#define UART_TX_STATUS_TX_BUSY_OFFSET 0x0       
#define UART_TX_STATUS_TX_BUSY_MASK 0x1       
/*
Transmit FIFO full
*/
#define UART_TX_STATUS_TX_FIFO_FULL_OFFSET 0x1       
#define UART_TX_STATUS_TX_FIFO_FULL_MASK 0x2       
/*
Transmit FIFO empty
*/
#define UART_TX_STATUS_TX_FIFO_EMPTY_OFFSET 0x2       
#define UART_TX_STATUS_TX_FIFO_EMPTY_MASK 0x4       


#endif /* UART_MMAP_H */