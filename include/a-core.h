// Automatically generated Thu Jun 13 10:15:09 EEST 2024

#ifndef A_CORE_MMAP_H
#define A_CORE_MMAP_H

#define A_CORE_PROGMEM        0x1000    

#define A_CORE_AXI4LSRAM      0x20000000

#define A_CORE_AXI4LGPIO      0x30000000

#define A_CORE_AXI4LUART      0x30001000

#define A_CORE_AXI4LDMA       0x30002000


#endif /* A_CORE_MMAP_H */