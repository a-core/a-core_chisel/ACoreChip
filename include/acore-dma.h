// Automatically generated Thu Jun 13 10:15:09 EEST 2024

#ifndef DMA_MMAP_H
#define DMA_MMAP_H

#define DMA_READ_START_ADDR                             0x0       

#define DMA_READ_END_ADDR                               0x4       

#define DMA_WRITE_START_ADDR                            0x8       

#define DMA_WRITE_END_ADDR                              0xc       

#define DMA_READ_INCREMENT                              0x10      

#define DMA_WRITE_INCREMENT                             0x14      

#define DMA_CONTROL                                     0x18      
#define DMA_CONTROL_INIT                                0x30000   
/*
Reset the DMA
*/
#define DMA_CONTROL_SOFT_RESET_OFFSET                   0x0       
#define DMA_CONTROL_SOFT_RESET_MASK                     0x1       
/*
Start the DMA transaction
*/
#define DMA_CONTROL_SOFTWARE_START_OFFSET               0x8       
#define DMA_CONTROL_SOFTWARE_START_MASK                 0x100     
/*
End condition:
00 -> LOOP - DMA loops read and write addresses until it's reset
01 -> READ - Transfer until read address reaches end address
10 -> WRITE - Transfer until write address reaches end address
11 -> EITHER - Transfer until either read or write reaches its end address
*/
#define DMA_CONTROL_END_CONDITION_OFFSET                0x10      
#define DMA_CONTROL_END_CONDITION_MASK                  0x30000   
#define DMA_CONTROL_END_CONDITION_INIT                  0x3       

#define DMA_STATUS                                      0x1c      
/*
Done signal indicating the DMA transfer has finished succesfully.
If end condition is loop (00), done signal will not be asserted.
*/
#define DMA_STATUS_DONE_OFFSET                          0x0       
#define DMA_STATUS_DONE_MASK                            0x1       

#define DMA_DELAY                                       0x20      
#define DMA_DELAY_INIT                                  0x101     
/*
Clock cycle delay between reads. Set 1 for highest throughput.
*/
#define DMA_DELAY_READ_DELAY_OFFSET                     0x0       
#define DMA_DELAY_READ_DELAY_MASK                       0xff      
#define DMA_DELAY_READ_DELAY_INIT                       0x1       
/*
Clock cycle delay between writes. Set 1 for highest throughput.
*/
#define DMA_DELAY_WRITE_DELAY_OFFSET                    0x8       
#define DMA_DELAY_WRITE_DELAY_MASK                      0xff00    
#define DMA_DELAY_WRITE_DELAY_INIT                      0x1       


#endif /* DMA_MMAP_H */