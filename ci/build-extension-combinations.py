#!/usr/bin/python3

import itertools
import subprocess
import os

supported_extensions = ["m"]

ext_strings = []
for i in range(len(supported_extensions)+1):
    for comb in itertools.combinations(supported_extensions, i):
        ext_strings.append(''.join(comb))

core_default_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "core-default.yml")
jtag_default_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "jtag-default.yml")

for ext in ext_strings:
    isa_string = "rv32i"+ext
    # Execute command
    print(f"- {isa_string}")
    output = subprocess.check_output(
        f"./configure && make ACoreChip isa_string={isa_string} core_config={core_default_path} jtag_config={jtag_default_path}",
        shell=True
    )
    print(output.decode('utf-8'))

